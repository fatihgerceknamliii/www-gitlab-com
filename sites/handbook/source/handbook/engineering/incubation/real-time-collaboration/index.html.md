---
layout: handbook-page-toc
title: Real-time Editing of Issue Descriptions (REID) Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## The Real-time Editing of Issue Descriptions (REID) Single-Engineer Group

The Real-time Editing of Issue Descriptions (REID) SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

This group is focussed on Real-time collaborative editing of issue descriptions and will build upon the recent work to [view real-time updates of assignee in issue sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/17589).

The goal is to have real-time collaborative editing of issue descriptions in order to turn issues into a Google Docs type of experience.  Initially we will dogfood this at GitLab for meeting agendas and notes.

There are additional ideas within the [Real-time collaboration Epic](https://gitlab.com/groups/gitlab-org/-/epics/2345), however the goal of this SEG is the minimal functionality described above.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/21473](https://gitlab.com/gitlab-org/gitlab/-/issues/21473)

### Vacancy

We’re currently hiring for this role and looking for someone that understands the underlying technologies used in real-time collaboration solutions in order to help design and develop this feature.  

You’ll need experience in bringing products to markets, experience with similar use cases, and experience with developing large scale services.  Ideally you've worked with WebSockets on Rails, and have experience (or at least knowledge and a keen interest) in [CFRDTs](https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type)

You should know the different technological approaches and open source solutions in this space, and be able to architect a scalable approach that is integrated into GitLab.  Our tech stack is Ruby, Go and Vue.js, and you’ll need to work across backend, frontend database and infrastructure to bring this opportunity to market.  

You can apply on our [careers page](https://about.gitlab.com/jobs/).

### Reading List

* [Hybrid Anxiety and Hybrid Optimism: The Near Future of Work](https://future.a16z.com/hybrid-anxiety-optimism-future-of-work/)
