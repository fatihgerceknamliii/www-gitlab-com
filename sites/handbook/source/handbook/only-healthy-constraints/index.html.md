---
layout: markdown_page
title: "Only Healthy Constraints"
description: "Companies often slow down as they mature. GitLab strives for healthy constraints."
canonical_path: "/handbook/only-healthy-constraints/"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Most companies regress to the mean and slow down over time. While some changes are required as a company grows and matures, not all change is inevitable or should be allowed to passively happen. As GitLab grows, we are conscious of how we operate and how it enables our ability to continue to operate with the agility of a startup. This is key to [efficiency](/handbook/values/#efficiency), enables [iteration](https://about.gitlab.com/handbook/values/#iteration), and helps us to drive [results](/handbook/values/#results).

## Importance of choosing to stay a startup

A senior candidate once referenced the "Letter From Our CEO" in [GitLab's S-1](https://www.sec.gov/Archives/edgar/data/0001653482/000162828021018818/gitlab-sx1.htm) (filed with the Securities and Exchange Commission on September 17, 2021). In it GitLab's CEO, Sid Sijbrandij, documented some of the ways that GitLab plans to avoid the stagnation experienced by most early stage companies as they mature. The person said, "your letter is good at reminding everyone that you are still a startup and need to retain that mindset. It's super hard, as you know. When I reflect upon all the amazing things I experienced at [Company X], it is a poorly managed company. The systems (legal, procurement, security) that grow with success are also designed to manage the downside. I see the same thing here at [Company Y]. I'm fighting to re-create the challenger mindset to reflect our market position outside of virtualization and networking. Cruft everywhere. You have an opportunity to minimize this as you scale."

We try to avoid the downside of maturation, because this better enables us to achieve results and mitigate many of our [greatest risks](/handbook/leadership/biggest-risks/). It also helps us to dodge many of the [coordination headwinds](https://komoroske.com/slime-mold/) that often plague more established companies. 

## Enabling only healthy constraints

1. We elevate [efficiency](/handbook/values/#efficiency) as one of our core values. 
1. To operate like a startup, we find ways to reinforce the startup ethos within the organization. We document the ways that we are [still a startup](/company/still-a-startup/). 
1. We share information through GitLab's [handbook](/handbook/). The handbook's over 2000 pages of text provide guidelines, not rules, for how team members should operate. Information is easily shared and accessible. If something needs to be updated, all team members (and even members of the wider-community) are empowered to suggest changes. 
1. We don't make key changes to how we operate just because it is what more mature companies have done. If the change challenges our ability to operate as a startup, we often quantify the impact of the change. For example, when we were preparing to become a public company, folks challenged GitLab's stance on transparency and focused on the risk of sharing information. We were able to quantify the significant value of transparency from team member efficiency, prospect awareness, and talent recruitment (among other things). Instead of discussing whether GitLab should be transparent, we were able to shift the conversation toward how to maintain responsible transparency.
1. We proactively recruit and hire team members who demonstrate an ability to excel in entrepreneurial work. We provide financial security and an environment in which they can continue to work with autonomy as [managers of one](/handbook/values/#managers-of-one).
1. We learn from more mature companies that have been successful at maintaining a startup ethos as they scale. Apple, for example, has been called the "world's largest startup." It manages tight headcount while achieving ambitious goals and sticking to timelines. This does cause burnout, which is something we actively watch and [try to mitigate](/company/culture/all-remote/mental-health/).
1. We try to be true to GitLab's [brand](/company/brand/): GitLab empowers everyone through knowledge access, job access, and the DevOps platform. Part of knowledge sharing involves being transparent beyond team members. An example of this is a list of 50 learnings and best practices from our CEO, CLO, and CFO from our IPO that we plan to publish in FY23-Q1. Another is GitLab being the first company to [publicly livestream its IPO](/company/history/#2021-10-years-of-gitlab), empowering anyone with an internet connection to share in the experience.
1. We look for ways to measure administrative burden. This helps us to understand and mitigate the impact of bureaucracy or administration on efficiency. 
1. We explore new ways to remove constraints. These constraints can be steps, limits, or approvals. Some challenges to constraints under consideration include:
    1. Make removing constraints someone's job
    1. Add as core part of the efficiency value
    1. Reward the best challenge to a constraint
    1. Constraints challenge escalation process
    1. Optimize for change. Focus on removing existing constraints instead of preventing new ones. Can remove recent constraints.
    1. Measure cycle time (not just for DevOps but also other processes that change states)
    1. Add relevant questions to team member engagement surveys. For example:
        1. Turnaround time of team members?
        1. To what extent does the handbook process constrain your day to day?
        1. Are we still a startup?
        1. To what extent do Finance/Legal/People/Security make your job more efficient?
1. We hire folks who are dedicated to GitLab's [mission](/company/mission/) and [vision](/company/mission/). [Pournelle's Iron Law of Bureaucracy](https://en.wikipedia.org/wiki/Jerry_Pournelle#Pournelle's_iron_law_of_bureaucracy) explains why this is important:

>In any bureaucratic organization there will be two kinds of people:
>
>First, there will be those who are devoted to the goals of the organization. Examples are dedicated classroom teachers in an educational bureaucracy, many of the engineers and launch technicians and scientists at NASA, even some agricultural scientists and advisors in the former Soviet Union collective farming administration.
>Secondly, there will be those dedicated to the organization itself. Examples are many of the administrators in the education system, many professors of education, many teachers union officials, much of the NASA headquarters staff, etc.
>The Iron Law states that in every case the second group will gain and keep control of the organization. It will write the rules, and control promotions within the organization.


